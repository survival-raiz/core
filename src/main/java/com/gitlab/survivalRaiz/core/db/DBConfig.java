package com.gitlab.survivalRaiz.core.db;

import api.skwead.storage.file.yml.YMLConfig;
import org.bukkit.plugin.java.JavaPlugin;

import java.io.IOException;
import java.util.Collections;
import java.util.Set;

public class DBConfig extends YMLConfig {
    public DBConfig(JavaPlugin plugin) {
        super("database", plugin);

        try {
            this.createConfig(Collections.singleton("path"));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void createConfig(Set<String> set) throws IOException {
        for (String param : set) {
            if (super.getConfig().get(param) == null) {
                super.getConfig().createSection(param);
                super.getConfig().set(param, "null");
                super.getConfig().save(super.getFile());
            }
        }
    }
}
