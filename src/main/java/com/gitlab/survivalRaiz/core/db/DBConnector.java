package com.gitlab.survivalRaiz.core.db;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;
import java.util.function.Function;

// https://docs.oracle.com/javase/tutorial/jdbc/basics/processingsqlstatements.html

/**
 * Handles connections to a sqlite database.
 */
class DBConnector {
    private final String db;

    private Connection connection = null;
    private Statement statement = null;

    public DBConnector(String db) {
        this.db = db;
    }

    /**
     * Executes a query on the database that returns a {@link ResultSet}.
     * @param query the query to be executed
     * @return the ResultSet from the query
     */
    // TODO: 7/28/20 As queries que não retornam ResultSet dão um Warn "query does not return ResultSet"
    private ResultSet runQuery(String query) {
        try {
            // create a database connection
            connection = DriverManager.getConnection("jdbc:sqlite:" + this.db);
            statement = connection.createStatement();
            statement.setQueryTimeout(30);  // set timeout to 30 sec.

            return statement.executeQuery(query);

        } catch (SQLException e) {
            // if the error message is "out of memory",
            // it probably means no database file is found
            System.err.println(e.getMessage());
        }
        return null;
    }

    protected void runAutoCloseableQuery(String query){
        try {
            // create a database connection
            connection = DriverManager.getConnection("jdbc:sqlite:" + this.db);
            statement = connection.createStatement();
            statement.setQueryTimeout(30);  // set timeout to 30 sec.

            statement.executeQuery(query);

        } catch (SQLException e) {
            // if the error message is "out of memory",
            // it probably means no database file is found
            System.err.println(e.getMessage());
        } finally {
            try {
                if (connection != null)
                    connection.close();

                if (statement != null)
                    statement.close();
            } catch (SQLException e) {
                // connection close failed.
                System.err.println(e.getMessage());
            }
        }
    }

    protected List<Object> run(String query, Function<ResultSet, List<Object>> operation){
        final ResultSet rs = runQuery(query);
        final List<Object> res = operation.apply(rs);
        close();

        return res;
    }

    private void close() {
        try {
            if (connection != null)
                connection.close();

            if (statement != null)
                statement.close();
        } catch (SQLException e) {
            // connection close failed.
            System.err.println(e.getMessage());
        }
    }
}
