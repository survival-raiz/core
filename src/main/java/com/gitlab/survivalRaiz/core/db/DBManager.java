package com.gitlab.survivalRaiz.core.db;

import org.bukkit.Bukkit;
import org.bukkit.Location;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.Function;

/**
 * Implements all queries to be used.
 * Also creates a config to store all data.
 */
public class DBManager extends DBConnector {

    public DBManager(DBConfig dbConfig) {
        super(dbConfig.getConfig().getString("path"));
    }

    public List<Object> fetchAllTable(String name, Function<ResultSet, List<Object>> operation) {
        return super.run(String.format("SELECT * FROM %s", name), operation);
    }

    /**
     * Searches for the given primary key value in the given table
     *
     * @param table the table name to look for
     * @param pk    the table's primary key
     * @param pkVal the value of the primary key to look up for
     * @return A {@link ResultSet} with all matches for the given parameters.
     */
    public List<Object> getFromDb(String table, String pk, Object pkVal, Function<ResultSet, List<Object>> operation) {
        return super.run(String.format("SELECT * FROM %s WHERE %s = '%s'", table, pk, pkVal), operation);
    }

    /**
     * Deletes the row corresponding to the primary key's value provided in the table
     *
     * @param table the table name to look for
     * @param pk    the table's primary key
     * @param pkVal the value of the primary key to look up for
     */
    public void remFromDb(String table, String pk, Object pkVal) {
        super.runAutoCloseableQuery(String.format("DELETE FROM %s WHERE %s = '%s'", table, pk, pkVal));
    }

    /**
     * Returns all of the player's terrains
     *
     * @param uuid the player's uuid
     * @return a {@link ResultSet} containing all terrains
     */
    public List<Object> getTerrainsByPlayer(UUID uuid, Function<ResultSet, List<Object>> operation) {
        return super.run(String.format("SELECT * FROM terrains WHERE owner = '%s'", uuid.toString()), operation);
    }

    /**
     * Query to add a terrain to the database. Sets the terrain's tax due date to 1 month from now.
     *
     * @param x     the center's x
     * @param y     the center's y
     * @param r     the terrain radius in chunks. It's half the length of the square containing the terrain.
     * @param owner the owner's UUID
     * @param id    the terrain's id
     */
    public void addTerrain(int x, int y, int r, UUID owner, int id) {
        super.runAutoCloseableQuery(String.format(
                "INSERT INTO terrains VALUES ('%s', '%s', '%s', '%s', '%s', '%s')",
                id, x, y, r, owner.toString(), LocalDate.now().plusMonths(1).toString()));
    }

    /**
     * Sets the terrain's tax due date to one month from now
     *
     * @param id the terrain's id
     */
    public void updateDueDate(int id) {
        super.runAutoCloseableQuery(String.format(
                "UPDATE terrains SET due='%s' WHERE terrainId='%s'",
                id, LocalDate.now().plusMonths(1).toString()
        ));
    }

    /**
     * Fetches the database to see how much money the player has
     *
     * @param player    The player's UUID
     * @param operation The operation to be executed on the player's balance.
     * @return it's balance.
     */
    public List<Object> getBalance(UUID player, Function<ResultSet, List<Object>> operation) {
        return super.run(String.format("SELECT amount FROM economy WHERE player = '%s'", player.toString()), operation);
    }

    /**
     * Updates a player's balance
     *
     * @param player the player whose balance is to be updated
     * @param newBal the player's new balance
     */
    public void setBalance(UUID player, double newBal) {
        super.runAutoCloseableQuery(
                String.format("UPDATE economy SET amount = %s WHERE player = '%s'", newBal, player));
    }

    /**
     * Creates the economy table entry for the player
     *
     * @param player The player to be added.
     */
    public void createAcc(UUID player) {
        super.runAutoCloseableQuery(String.format("INSERT INTO economy VALUES ('%s', 0)", player));
    }

    /**
     * Calculates how many shops are there buying/selling the item.
     *
     * @param hash The {@link org.bukkit.inventory.ItemStack}'s hash to be searched for
     *             (calculated from the EconomyConfig method)
     * @return An integer telling how much shops buy/sell said item.
     */
    public int getShopsForItem(String hash) {
        final AtomicInteger count = new AtomicInteger();

        super.run(String.format("SELECT item FROM shops WHERE item='%s'", hash), resultSet -> {
            try {
                while (resultSet.next())
                    count.incrementAndGet();
            } catch (SQLException e) {
                return null;
            }
            return null;
        });

        return count.get();
    }

    /**
     * Gets all shops that operate the item with the given hash
     *
     * @param hash      The {@link org.bukkit.inventory.ItemStack}'s hash to be searched for
     *                  (calculated from the EconomyConfig method)
     * @param operation The operation to be qdone on the query's result
     */
    public void shopsOfItem(String hash, Function<ResultSet, List<Object>> operation) {
        super.run(String.format("SELECT * FROM shops WHERE item='%s'", hash), operation);
    }

    /**
     * Counts the shops a player has
     *
     * @param player the shop owner
     * @return the number of shops said player owns
     */
    public int getShopsFromPlayer(UUID player) {
        final AtomicInteger count = new AtomicInteger();

        super.run(String.format("SELECT owner FROM shops WHERE owner='%s'", player), resultSet -> {
            try {
                while (resultSet.next())
                    count.incrementAndGet();
            } catch (SQLException e) {
                return null;
            }
            return null;
        });

        return count.get();
    }

    /**
     * Adds a shop to the database
     *
     * @param owner    the shop owner's UUID
     * @param itemHash the hash of the item
     * @param q        the amount to be handled by the shop
     * @param c        the buy price
     * @param v        the sell price
     * @param x        the x coordinate of the sign
     * @param y        the y coordinate of the sign
     * @param z        the z coordinate of the sign
     */
    public void addShop(UUID owner, String itemHash, int q, double c, double v, int x, int y, int z) {
        boolean adm = false;

        if (Bukkit.getPlayer(owner) != null)
            adm = Bukkit.getPlayer(owner).hasPermission("sr.admin-shop");

        System.out.println(String.format("INSERT INTO shops VALUES('%s', '%s', '%s', %s, %s, %s, %s, %s, %s)",
                owner, adm, itemHash, q, c, v, x, y, z));

        super.runAutoCloseableQuery(
                String.format("INSERT INTO shops VALUES('%s', '%s', %s, %s, %s, %s, %s, %s)",
                        owner, itemHash, q, c, v, x, y, z));
    }

    /**
     * Tells if a certain location is a shop or not
     *
     * @param x the location's x
     * @param y the location's y
     * @param z the location's z
     * @return true if the shops' table contains those coordinates
     */
    public boolean isShop(int x, int y, int z) {
        final List<Object> shops = new ArrayList<>();

        super.run(String.format("SELECT * FROM shops WHERE x=%s AND y=%s AND z=%s", x, y, z),
                resultSet -> {
                    try {
                        while (resultSet.next())
                            shops.add(1);
                    } catch (SQLException e) {
                        return null;
                    }
                    return null;
                });

        return shops.size() > 0;
    }

    /**
     * Removes the shop at the defined location
     *
     * @param x the location's x
     * @param y the location's y
     * @param z the location's z
     */
    public void remShop(int x, int y, int z) {
        super.runAutoCloseableQuery(String.format("DELETE FROM shops WHERE x=%s AND y=%s AND z=%s", x, y, z));
    }

    /**
     * Tells whose shop is the shop at the given location
     *
     * @param location the shop's location
     * @return the owner's UUID, or null if it is not a shop
     */
    public UUID getShopOwner(Location location) {
        final List<Object> shop = new ArrayList<>();

        super.run(String.format("SELECT owner FROM shops WHERE x=%s AND y=%s AND z=%s",
                location.getBlockX(), location.getBlockY(), location.getBlockZ()),
                resultSet -> {
                    try {
                        while (resultSet.next())
                            shop.add(resultSet.getString(1));
                    } catch (SQLException e) {
                        return null;
                    }
                    return null;
                });

        try {
            return UUID.fromString((String) shop.get(0));
        } catch (ArrayIndexOutOfBoundsException e) {
            return null;
        }
    }

    /**
     * Gets the sell price of the shop at said location
     *
     * @param location the shop's location
     * @return the sell price of the shop
     */
    public double getSellPrice(Location location) {
        final List<Object> shop = new ArrayList<>();

        super.run(String.format("SELECT sell FROM shops WHERE x=%s AND y=%s AND z=%s",
                location.getBlockX(), location.getBlockY(), location.getBlockZ()),
                resultSet -> {
                    try {
                        while (resultSet.next())
                            shop.add(resultSet.getDouble(1));
                    } catch (SQLException e) {
                        return null;
                    }
                    return null;
                });

        try {
            return (double) shop.get(0);
        } catch (ArrayIndexOutOfBoundsException e) {
            return 0;
        }
    }

    /**
     * Gets the buy price of the shop at said location
     *
     * @param location the shop's location
     * @return the sell price of the shop
     */
    public double getBuyPrice(Location location) {
        final List<Object> shop = new ArrayList<>();

        super.run(String.format("SELECT buy FROM shops WHERE x=%s AND y=%s AND z=%s",
                location.getBlockX(), location.getBlockY(), location.getBlockZ()),
                resultSet -> {
                    try {
                        while (resultSet.next())
                            shop.add(resultSet.getDouble(1));
                    } catch (SQLException e) {
                        return null;
                    }
                    return null;
                });

        try {
            return (double) shop.get(0);
        } catch (ArrayIndexOutOfBoundsException e) {
            return 0;
        }
    }

    /**
     * Gets the amount price of the shop at said location
     *
     * @param location the shop's location
     * @return the sell price of the shop
     */
    public int getAmount(Location location) {
        final List<Object> shop = new ArrayList<>();

        super.run(String.format("SELECT amount FROM shops WHERE x=%s AND y=%s AND z=%s",
                location.getBlockX(), location.getBlockY(), location.getBlockZ()),
                resultSet -> {
                    try {
                        while (resultSet.next())
                            shop.add(resultSet.getInt(1));
                    } catch (SQLException e) {
                        return null;
                    }
                    return null;
                });

        try {
            return (int) shop.get(0);
        } catch (ArrayIndexOutOfBoundsException e) {
            return 0;
        }
    }

    /**
     * gets the item hash of the item transactioned in the given location
     *
     * @param location the location of the shop
     * @return the hash
     */
    public String getItemHash(Location location) {
        final List<Object> shop = new ArrayList<>();

        super.run(String.format("SELECT item FROM shops WHERE x=%s AND y=%s AND z=%s",
                location.getBlockX(), location.getBlockY(), location.getBlockZ()),
                resultSet -> {
                    try {
                        while (resultSet.next())
                            shop.add(resultSet.getString(1));
                    } catch (SQLException e) {
                        return null;
                    }
                    return null;
                });

        try {
            return (String) shop.get(0);
        } catch (ArrayIndexOutOfBoundsException e) {
            return null;
        }
    }

    /**
     * Tells how many clans are there with the given name.
     * In practice will return 0 or 1.
     *
     * @param name The name to be tested
     * @return 0 if no clan is found, 1 if there is any, more if something went wrong.
     */
    public int getClansByName(String name) {
        final AtomicInteger count = new AtomicInteger();

        super.run(String.format("SELECT x FROM clans WHERE name='%s'", name),
                resultSet -> {
                    try {
                        while (resultSet.next())
                            count.getAndIncrement();
                    } catch (SQLException | NullPointerException e) {
                        return null;
                    }
                    return null;
                });

        return count.get();
    }

    /**
     * Fetches the clan's base coordinates and converts into a {@link Location}
     *
     * @param clan The clan's name
     * @return The base's location
     */
    public Location getClanBase(String clan) {
        final List<Object> data = super.run(String.format("SELECT * FROM clans WHERE name='%s'", clan),
                resultSet -> {
                    final List<Object> dat = new ArrayList<>();

                    try {
                        while (resultSet.next()) {
                            if (resultSet.getString(6) == null)
                                return dat;

                            dat.add(new Location(Bukkit.getWorld(resultSet.getString(6)),
                                    resultSet.getInt(3),
                                    resultSet.getInt(4),
                                    resultSet.getInt(5)));
                        }
                    } catch (SQLException | NullPointerException e) {
                        return dat;
                    }
                    return dat;
                });

        try {
            return (Location) data.get(0);
        } catch (IndexOutOfBoundsException e) {
            return null;
        }
    }

    /**
     * Tells if the player has a clan
     *
     * @param player The player to be tested
     * @return an array with the form [clan, rank] if the player has a clan, null otherwise.
     */
    public String[] hasClan(UUID player) {
        final List<Object> data = new ArrayList<>();

        super.run(String.format("SELECT clan, rank FROM membership WHERE player='%s'", player.toString()),
                resultSet -> {
                    try {
                        while (resultSet.next()) {
                            data.add(resultSet.getString(1));
                            data.add(resultSet.getString(2));
                        }
                    } catch (SQLException e) {
                        return null;
                    }
                    return null;
                });

        try {
            return new String[]{(String) data.get(0), (String) data.get(1)};
        } catch (IndexOutOfBoundsException e) {
            return null;
        }
    }

    /**
     * Creates a clan given the name and tag
     *
     * @param name The clan name
     * @param tag  The clan tag
     */
    public void createClan(String name, String tag) {
        super.runAutoCloseableQuery(String.format("INSERT INTO clans (name, tag) VALUES ('%s', '%s')", name, tag));
    }

    /**
     * Updates a clan's base
     *
     * @param clan  The clan whose base is to be updated
     * @param x     The x of the base
     * @param y     The y of the base
     * @param z     The z of the base
     * @param world The world name
     */
    public void updateClanBase(String clan, double x, double y, double z, String world) {
        super.runAutoCloseableQuery(String.format(
                "UPDATE clans SET x=%s, y=%s, z=%s, world='%s' WHERE name = '%s'",
                x, y, z, world, clan));
    }

    /**
     * Adds a player to a clan
     *
     * @param player   The player to be added
     * @param clan     The clan that recruited the player
     * @param rankName The player rank's name
     */
    public void addToClan(UUID player, String clan, String rankName) {
        super.runAutoCloseableQuery(String.format("INSERT INTO membership VALUES ('%s', '%s', '%s')",
                clan, player.toString(), rankName));
    }

    /**
     * Fetches the player's rank on the clan.
     *
     * @param player The player to be tested
     * @return the player's rank on the clan, null if they have no clan
     */
    public String getClanRank(UUID player) {
        final StringBuilder rank = new StringBuilder();

        super.run(String.format("SELECT rank FROM membership WHERE player = '%s'", player.toString()),
                resultSet -> {
                    try {
                        rank.append(resultSet.getString(1));
                    } catch (SQLException e) {
                        e.printStackTrace();
                    }

                    return null;
                });

        return rank.toString();
    }

    /**
     * Sets the player's clan rank
     *
     * @param player The player's UUID
     * @param rank   The new rank
     */
    public void setClanRank(UUID player, String rank) {
        super.runAutoCloseableQuery(String.format(
                "UPDATE membership SET rank='%s' WHERE player='%s'",
                rank, player.toString()));
    }

    /**
     * Deletes a clan and removes all the members
     *
     * @param clan The clan to be deleted
     */
    public void deleteClan(String clan) {
        super.runAutoCloseableQuery(String.format("DELETE FROM clans WHERE name = '%s'", clan));

        super.runAutoCloseableQuery(String.format("DELETE FROM membership WHERE clan = '%s'", clan));
    }

    /**
     * Removes a member from the clan. In practice this removes any membership the player has,
     * before using this method it may be required to validate the player's clan.
     *
     * @param player The member to be removed
     */
    public void removeMember(UUID player) {
        super.runAutoCloseableQuery(String.format("DELETE FROM membership WHERE player = '%s'", player.toString()));
    }

    /**
     * Tells when the player will be able to claim said kit again
     *
     * @param player The player that is trying to get the kit
     * @param kit    The kit the player is trying to get
     * @return The {@link LocalDateTime} when the player can claim the kit again.
     * If he never claimed that kit the date returned will be {@code LocalDateTime.now().minusNanos(1)}
     */
    public LocalDateTime getKitReadyTime(UUID player, String kit) {
        final List<Object> time = super.run(String.format("SELECT date FROM kits WHERE player='%s' AND kit='%s'", player, kit),
                resultSet -> {
                    try {
                        return Collections.singletonList(LocalDateTime.parse(resultSet.getString(1)));
                    } catch (SQLException | NullPointerException e) {
                        return null;
                    }
                });

        return time == null ? LocalDateTime.now().minusNanos(1) : (LocalDateTime) time.get(0);
    }

    /**
     * Sets the next time the player will be able to get the kit. Deletes the previous entry if it exists.
     *
     * @param player    The player that just got the kit
     * @param kit       The kit the player got
     * @param readyTime The next time the kit will be available
     */
    public void setKitReadyDate(UUID player, String kit, LocalDateTime readyTime) {
        super.runAutoCloseableQuery(String.format("DELETE FROM kits WHERE player='%s' AND kit='%s'",
                player.toString(), kit));

        super.runAutoCloseableQuery(String.format("INSERT INTO kits VALUES ('%s', '%s', '%s')",
                player.toString(), kit, readyTime.toString()));
    }

    /**
     * Query to create the tables if they do not exist.
     */
    public void generate() {
        super.runAutoCloseableQuery(
                "CREATE TABLE IF NOT EXISTS terrains(" +
                        "terrainId integer NOT NULL," +
                        "x integer NOT NULL," +
                        "y integer NOT NULL," +
                        "r integer NOT NULL," +
                        "owner varchar(24) NOT NULL," +
                        "admin boolean NOT NULL" +
                        "due text," +
                        "PRIMARY KEY(terrainId)" +
                        ");");

        super.runAutoCloseableQuery(
                "CREATE TABLE IF NOT EXISTS economy(" +
                        "player varchar(24) NOT NULL," +
                        "amount real NOT NULL" +
                        ")");

        super.runAutoCloseableQuery(
                "CREATE TABLE IF NOT EXISTS shops(" +
                        "owner varchar(24) NOT NULL," +
                        "item text NOT NULL," +
                        "amount integer NOT NULL," +
                        "buy real," +
                        "sell real," +
                        "x integer NOT NULL," +
                        "y integer NOT NULL," +
                        "z integer NOT NULL" +
                        ")");

        super.runAutoCloseableQuery(
                "CREATE TABLE IF NOT EXISTS clans(" +
                        "name text NOT NULL," +
                        "tag varchar(3) NOT NULL," +
                        "x integer," +
                        "y integer," +
                        "z integer," +
                        "world text," +
                        "PRIMARY KEY(name)" +
                        ")");

        super.runAutoCloseableQuery(
                "CREATE TABLE IF NOT EXISTS membership(" +
                        "clan text NOT NULL," +
                        "player varchar(24) NOT NULL," +
                        "rank text NOT NULL," +
                        "PRIMARY KEY(player)" +
                        ")");

        super.runAutoCloseableQuery(
                "CREATE TABLE IF NOT EXISTS kits(" +
                        "player varchar(24) NOT NULL," +
                        "kit text NOT NULL," +
                        "date text NOT NULL" +
                        ")");
    }
}