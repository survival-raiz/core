package com.gitlab.survivalRaiz.core.gui;

import org.bukkit.Material;

import java.util.List;
import java.util.Map;

public class GUIData {
    private final String name;
    private final int rows;
    private final List<String> layout;
    private final Material borderMaterial;
    private final String borderName;
    private final Material nextMaterial;
    private final String nestName;
    private final Material prevMaterial;
    private final String prevName;
    private final Map<Material, String> content;

    public GUIData(String name, int rows, List<String> layout, Material borderMaterial, String borderName, Material nextMaterial, String nestName, Material prevMaterial, String prevName, Map<Material, String> content) {
        this.name = name;
        this.rows = rows;
        this.layout = layout;
        this.borderMaterial = borderMaterial;
        this.borderName = borderName;
        this.nextMaterial = nextMaterial;
        this.nestName = nestName;
        this.prevMaterial = prevMaterial;
        this.prevName = prevName;
        this.content = content;
    }

    public GUIData() {
        this.name = null;
        this.rows = 1;
        this.layout = null;
        this.borderMaterial = null;
        this.borderName = null;
        this.nextMaterial = null;
        this.nestName = null;
        this.prevMaterial = null;
        this.prevName = null;
        this.content = null;
    }
}
