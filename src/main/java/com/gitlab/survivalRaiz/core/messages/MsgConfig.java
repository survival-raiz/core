package com.gitlab.survivalRaiz.core.messages;

import api.skwead.storage.file.yml.YMLConfig;
import org.bukkit.plugin.java.JavaPlugin;

import java.io.IOException;
import java.util.Set;

/**
 * Handles all message related storage
 */
public class MsgConfig extends YMLConfig {

    public MsgConfig(String name, JavaPlugin plugin) {
        super(name, plugin);
    }

    @Override
    public void createConfig(Set<String> set) throws IOException {
//        if (super.getConfig().get(msgFormat) == null)
//            super.getConfig().set(msgFormat, "null");

        for (String msg : set) {
            if (super.getConfig().get(msg + ".text") == null) {
                super.getConfig().createSection(msg + ".text");
                super.getConfig().set(msg + ".text", new String[]{msg});
                super.getConfig().save(super.getFile());
            }
            if (super.getConfig().get(msg + ".type") == null) {
                super.getConfig().createSection(msg + ".type");
                super.getConfig().set(msg + ".type", "message");
                super.getConfig().save(super.getFile());
            }
        }
    }
}
