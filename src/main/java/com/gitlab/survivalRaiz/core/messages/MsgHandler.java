package com.gitlab.survivalRaiz.core.messages;

import me.clip.placeholderapi.PlaceholderAPI;
import org.bukkit.OfflinePlayer;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;

import java.io.IOException;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;
import java.util.function.Function;

/**
 * Manages message sending. Modularized via config
 */
public class MsgHandler {
    private final MsgConfig msgs;
    private final TemplateConfig templates;

    public MsgHandler(JavaPlugin plugin) {
        this.msgs = new MsgConfig("messages", plugin);
        this.templates = new TemplateConfig("templates", plugin);

        final Set<String> yeet = new HashSet<>();
        for (int i = 0; i < Message.values().length; i++) {
            yeet.add(Message.values()[i].name());
        }

        try {
            templates.createConfig(MsgFormat.getAllTypes());
            msgs.createConfig(yeet);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Sends a message to a single receiver by calling {@link MsgHandler#message(Set, Message)},
     * passing {@link Collections#singleton(Object)} of T
     *
     * @param receiver Who receives the message
     * @param message  The message to be sent
     * @param <T>      The type of the receiver,
     */
    public <T extends CommandSender> void message(T receiver, Message message) {
        message(Collections.singleton(receiver), message);
    }

    /**
     * Sends a message to a group of {@link CommandSender}
     *
     * @param receivers a set with everyone that will recieve the message
     * @param message   the message to be sent
     */
    public void message(Set<? extends CommandSender> receivers, Message message) {
        final String type = this.msgs.getConfig().getString(message.name() + ".type");
        final String[] msg = this.msgs.getConfig().getStringList(message.name() + ".text").toArray(new String[0]);
        if (type == null) return;
        final String format = this.templates.getConfig().getString(type);
        if (format == null) return;

        // TODO: 9/2/20 Verificar o pq de ter 2 fors iguais anestados
        for (int i = 0; i < receivers.size(); i++)
            for (CommandSender cs : receivers) {
                if (!(cs instanceof Player))
                    for (String s : msg)
                        cs.sendMessage(format
                                .replaceAll("&", "§")
                                .replaceAll("%msg%", s)
                                .replaceAll("&", "§"));
                else
                    for (String s : msg)
                        cs.sendMessage(format
                                .replaceAll("&", "§")
                                .replaceAll("%msg%", PlaceholderAPI.setPlaceholders(
                                        ((Player) cs).isOnline() ? (Player) cs : (OfflinePlayer) cs, s))
                                .replaceAll("&", "§"));
            }

//        message(receivers, message, (Consumer<String>) null);
    }

    /**
     * Sends a message to a single receiver by calling {@link MsgHandler#message(Set, Message)},
     * passing {@link Collections#singleton(Object)} of T.
     * Aplies all ops to the resulting string, for when using PAPI placeholders is not desired.
     *
     * @param receiver Who receives the message
     * @param message  The message to be sent
     * @param <T>      The type of the receiver
     * @param ops      The operations to be aplies to the message
     */
    public <T extends CommandSender> void message(T receiver, Message message, Function<String, String>... ops) {
        message(Collections.singleton(receiver), message, ops);
    }

    /**
     * Sends a message whose content can be processed, in case working with PAPI is not desired
     *
     * @param receivers a set with everyone that will recieve the message
     * @param message   the message to be sent
     * @param ops       all the changes to be made on the message
     */
    public void message(Set<? extends CommandSender> receivers, Message message, Function<String, String>... ops) {
        final String type = this.msgs.getConfig().getString(message.name() + ".type");
        final String[] msg = this.msgs.getConfig().getStringList(message.name() + ".text").toArray(new String[0]);
        if (type == null) return;
        final String format = this.templates.getConfig().getString(type);
        if (format == null) return;

        // TODO: 9/2/20 Verificar o pq de ter 2 fors iguais anestados
        for (int i = 0; i < receivers.size(); i++) {
            for (CommandSender cs : receivers) {

                if (!(cs instanceof Player))
                    for (int j = 0; j < msg.length; j++)
                        msg[j] = format
                                .replaceAll("&", "§")
                                .replaceAll("%msg%", msg[j])
                                .replaceAll("&", "§");
                else
                    for (int j = 0; j < msg.length; j++)
                        msg[j] = format
                                .replaceAll("&", "§")
                                .replaceAll("%msg%", PlaceholderAPI.setPlaceholders(
                                        ((Player) cs).isOnline() ? (Player) cs : (OfflinePlayer) cs, msg[j]))
                                .replaceAll("&", "§");

                for (int j = 0; j < msg.length; j++) {
                    for (int k = 0; k < ops.length; k++)
                        msg[j] = ops[k].apply(msg[j]);

                    cs.sendMessage(msg[j]);
                }
            }
        }
    }

    public MsgConfig getMsgs() {
        return msgs;
    }

    public TemplateConfig getTemplates() {
        return templates;
    }
}
